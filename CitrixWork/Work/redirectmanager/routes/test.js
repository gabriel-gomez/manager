/**
 * Created by Gabe on 6/8/17.
 */

var express = require('express');
var router = express.Router();
var validate = require('../services/validateService');

router.post('/new', function(req, res) {
    console.log(req.query);
    validate(req.query.issueKey);
});

module.exports = router;