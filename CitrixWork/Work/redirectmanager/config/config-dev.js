/**
 * Created by Gabe on 6/8/17.
 */

module.exports = {
    jira: {
    username: "robot",
        password: 'M6rvzYgbjENoM7Emygwn',
        host: 'citrixwebteam2.atlassian.net',
        port: '443',
        customfields: {
            redirectFrom: "customfield_15000",
            redirectTo: "customfield_15001",
            startDate: "customfield_14401",
            endDate: "customfield_14402",
        },
        transitions: {
            validated: "11",
            error: "51",
            rejected: "21",
            implemented: "31",
            removed: "41",
            reopened: "61",
        }
    },

    logDirectory: './Logs/',
    logFileName: 'app.log',
    expetionLogFile: 'exceptions.log'
};