/**
 * Created by Gabe on 6/9/17.
 */

var winston = require('winston');
var fs = require('fs');
var config = require('../config/config-dev');



if (!fs.existsSync(config.logDirectory)) {
    fs.mkdirSync(config.logDirectory);
}


var logger = new (winston.Logger) ({
    transports: [
        new winston.transports.Console ({level: 'debug', colorize: true}),
        new winston.transports.File ({filename:  config.logDirectory + config.logFileName, level: 'debug'})
    ],
    execptionHandlers: [
        new winston.transports.Console ({humanReadableUnhandledException: true, colorize: true}),
        new winston.transports.File({filename: config.logDirectory + config.expetionLogFile})
    ]
});

module.exports = logger;