/**
 * Created by Gabe on 6/8/17.
 */
var jira = require('./jiraService');
var logger = require('./loggerService');


/**
 * This returns the start date, end date, source url and destination url of the jira issue.
 *
 * @param issueId - JIRA ticket keys
 */
var random = function(issueId) {
    jira.findIssue(issueId, function(err, res) {
        if (err) {
            logger.error(err);
            return;
        }
        logger.debug(res);

        var sourceUrl = res.redirectFrom; // Setting the source URL
        var finalDestination = res.redirectTo; // Setting the anticipated final destination

        jira.searchJira(issueId, sourceUrl,  function(err, res) {
            if (err) {
                logger.error('The error is called here:', err);
                return;
            }
            logger.debug('Response from searchJira function:',res);

            var jqlResponseTotal = res.total;
            var issueKey = res.issues[0].key;

            jira.determiner(issueId, issueKey, jqlResponseTotal, sourceUrl, finalDestination, function(err, res){

                if (err) {
                    logger.error(err);
                    return;
                }

                logger.debug(res);
                console.log(res);

                if (res === 'Finished') {

                }
            });
        });
    });
};

module.exports = random;